package homework5

fun main() {
    println("Введите целое число:")
    try {
        val digit = readln().toInt()
        println(revert(digit))
    } catch (e: NumberFormatException) {
        println("Введено не целое число")
    }
}

fun revert(digit: Int): Int {
    return digit.toString().reversed().toInt()
}
