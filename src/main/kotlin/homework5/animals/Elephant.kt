package homework5.animals

class Elephant(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("grass", "banana")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}
