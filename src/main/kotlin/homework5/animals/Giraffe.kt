package homework5.animals

class Giraffe(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("tree")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}