package homework5.animals

class Chimpanzee(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("banana", "apple")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}