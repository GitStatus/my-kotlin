package homework5.animals

class Gorilla(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("banana", "wood")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}