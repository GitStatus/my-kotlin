package homework5.animals

class Lion(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("meat", "beef")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}