package homework5.animals

class Hippo(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("watermelon", "candies", "grass")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}