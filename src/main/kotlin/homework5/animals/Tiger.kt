package homework5.animals

class Tiger(val name: String, val height: Int, val weight: Int) {
    val foodPrefs = arrayOf("meat", "beef", "deer")
    var full = 0

    fun eat(food: String) {
        if (food in foodPrefs) full++
    }
}