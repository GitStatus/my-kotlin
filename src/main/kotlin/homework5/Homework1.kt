package homework5

import homework5.animals.*

fun main() {

    val myLion = Lion("Лев", 1, 2)
    myLion.eat("meat")
    println("Сытость льва: " + myLion.full)

    val myTiger = Tiger("Тигр", 2, 3)
    myTiger.eat("deer")
    println("Сытость тигра: " + myTiger.full)

    val myHippo = Hippo("Бегемот", 3, 4)
    myHippo.eat("watermelon")
    myHippo.eat("candies")
    myHippo.eat("grass")
    println("Сытость бегемота: " + myHippo.full)

    val myWolf = Wolf("Волк", 4, 5)
    myWolf.eat("grass") // съел траву, но не наелся
    println("Сытость волка: " + myWolf.full)

    val myGiraffe = Giraffe("Жираф", 5, 6)
    myGiraffe.eat("tree")
    println("Сытость жирафа: " + myGiraffe.full)

    val myElephant = Elephant("Слон", 6, 7)
    myElephant.eat("grass")
    println("Сытость слона: " + myElephant.full)

    val myChimpanzee = Chimpanzee("Шимпанзе", 7, 8)
    myChimpanzee.eat("banana")
    myChimpanzee.eat("apple")
    println("Сытость шимпанзе: " + myChimpanzee.full)

    val myGorilla = Gorilla("Горилла", 8, 9)
    myGorilla.eat("wood")
    println("Сытость гориллы: " + myGorilla.full)

}


