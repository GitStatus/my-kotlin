package homework6

abstract class Animal(open val name: String, open val height: Int, open val weight: Int) {
    abstract val foodPrefs: Array<String>
    var full: Int = 0

    fun eat(food: String) {
        if (foodPrefs.contains(food)) full++
    }
}

fun feedAllAnimals(animals: Array<Animal>, favouriteFood: Array<String>) {
    for (animal in animals) {
        for (food in favouriteFood) {
            animal.eat(food)
        }
        println("Сытость ${animal.name} = ${animal.full}")
    }
}