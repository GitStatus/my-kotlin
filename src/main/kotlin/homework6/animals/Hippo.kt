package homework6.animals

import homework6.Animal

class Hippo(name: String, height: Int, weight: Int) : Animal(name, height, weight) {

    override val foodPrefs: Array<String> = arrayOf("watermelon", "candies", "grass", "banana")
}