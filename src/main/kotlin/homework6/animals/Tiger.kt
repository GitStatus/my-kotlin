package homework6.animals

import homework6.Animal

class Tiger(name: String, height: Int, weight: Int) : Animal(name, height, weight) {

    override val foodPrefs: Array<String> = arrayOf("meat", "beef", "deer")
}