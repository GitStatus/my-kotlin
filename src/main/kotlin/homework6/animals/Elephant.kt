package homework6.animals

import homework6.Animal

class Elephant(name: String, height: Int, weight: Int) : Animal(name, height, weight) {

    override val foodPrefs: Array<String> = arrayOf("grass", "banana")
}
