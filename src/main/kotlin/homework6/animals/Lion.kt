package homework6.animals

import homework6.Animal

class Lion(name: String, height: Int, weight: Int) : Animal(name, height, weight) {

    override val foodPrefs: Array<String> = arrayOf("meat", "beef")
}
