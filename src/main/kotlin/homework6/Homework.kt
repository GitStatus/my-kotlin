package homework6

import homework6.animals.*

fun main() {
    val myLion = Lion("Лев", 1, 2)
    val myTiger = Tiger("Тигр", 2, 3)
    val myHippo = Hippo("Бегемот", 3, 4)
    val myWolf = Wolf("Волк", 4, 5)
    val myGiraffe = Giraffe("Жираф", 5, 6)
    val myElephant = Elephant("Слон", 6, 7)
    val myChimpanzee = Chimpanzee("Шимпанзе", 7, 8)
    val myGorilla = Gorilla("Горилла", 8, 9)

    val animals = arrayOf(myLion, myTiger, myHippo, myWolf, myGiraffe, myElephant, myChimpanzee, myGorilla)
    val food = arrayOf("meat", "grass", "banana", "candies", "deer", "beef", "apple", "tree", "wood")

    feedAllAnimals(animals, food)
}