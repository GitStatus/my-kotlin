package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    var two = 0
    var three = 0
    var four = 0
    var five = 0

    for (mark in marks) {
        when (mark) {
            2 -> two++
            3 -> three++
            4 -> four++
            5 -> five++
        }
    }

    println("Отличников - ${String.format("%.1f", five.toDouble() / marks.size * 100)}%")
    println("Хорошистов - ${String.format("%.1f", four.toDouble() / marks.size * 100)}%")
    println("Троечников - ${String.format("%.1f", three.toDouble() / marks.size * 100)}%")
    println("Двоечников - ${String.format("%.1f", two.toDouble() / marks.size * 100)}%")
}