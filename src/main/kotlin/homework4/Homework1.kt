package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)

    var joeCoins = 0
    var teamCoins = 0

    for (coin in myArray) {
        if (coin % 2 == 0) {
            joeCoins++
        } else teamCoins++
    }

    println("Джо забирает себе $joeCoins монет чётного номинала")
    println("Команда забирает себе $teamCoins монет нечётного номинала")
}
