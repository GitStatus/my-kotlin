package homework8

val userCart = mutableMapOf(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf(
    "milk",
    "bread",
    "sugar"
)
const val discountValue = 0.20
val vegetableSet = setOf(
    "potato",
    "tomato",
    "onion",
    "cucumber"
)
val prices = mutableMapOf(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей: ${countVegetables()}")
    println("Итого: ${calculateTotalPrice()}")
}

fun countVegetables(): Int {
    var quantity = 0
    userCart.forEach {
        if (vegetableSet.contains(it.key)) {
            quantity += it.value
        }
    }
    return quantity
}

fun calculateTotalPrice(): Double {
    var totalPrice = 0.0
    userCart.forEach {
        totalPrice += if (discountSet.contains(it.key)) {
            prices.getOrDefault(it.key, 0.0) * it.value * (1 - discountValue)
        } else {
            prices.getOrDefault(it.key, 0.0) * it.value
        }
    }
    return totalPrice
}
