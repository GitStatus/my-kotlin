package homework2

fun main() {
    val volumeOfLimonade: Int = 18500
    val volumeOfPinaColada: Short = 200
    val volumeOfWiskey: Byte = 50
    val volumeOfFresh: Long = 3000000000
    val volumeOfCola: Float = 0.5f
    val volumeOfAle: Double = 0.666666667
    val authorsDrink: String = "Что-то авторское!"

    println("Заказ - '$volumeOfLimonade мл лимонада' готов!")
    println("Заказ - '$volumeOfPinaColada мл пина колады' готов!")
    println("Заказ - '$volumeOfWiskey мл виски' готов!")
    println("Заказ - '$volumeOfFresh капель фреша' готов!")
    println("Заказ - '$volumeOfCola литра колы' готов!")
    println("Заказ - '$volumeOfAle литра эля' готов!")
    println("Заказ - '$authorsDrink' готов!")
}