package homework7

class WrongPasswordException(message: String) : Exception(message)