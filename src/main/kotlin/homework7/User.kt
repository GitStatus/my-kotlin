package homework7

class User(val username: String, val password: String)

fun createUser(username: String, password: String, passwordConfirmation: String): User {
    if (username.length > 20) {
        throw WrongLoginException("Логин не должен превышать 20 символов")
    }

    if (password.length < 10) {
        throw WrongPasswordException("Пароль должен содержать не менее 10 символов")
    }

    if (password != passwordConfirmation) {
        throw WrongPasswordException("Пароль и подтверждение пароля не совпадают")
    }

    return User(username, password)
}