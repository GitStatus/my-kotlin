package homework7

class WrongLoginException(message: String) : Exception(message)