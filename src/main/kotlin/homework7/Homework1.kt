package homework7

fun main() {
    println("Введите логин")
    val username = readln()

    println("Введите пароль")
    val password = readln()

    println("Повторите пароль")
    val passwordConfirmation = readln()

    try {
        val newUser = createUser(username, password, passwordConfirmation)
        println("Пользователь ${newUser.username} с паролем ${newUser.password} создан успешно")
    } catch (e: WrongLoginException) {
        println("Ошибка при регистрации: ${e.message}")
    } catch (e: WrongPasswordException) {
        println("Ошибка при регистрации: ${e.message}")
    }
}